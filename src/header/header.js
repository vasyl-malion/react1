import React, {useState} from "react";
import style from './header.css';

export const Header = ({data}) => {

    const [users, setUsers] = useState([])
    // const users = [];

    const countUsers = () => {

        // data.map(item => users.push(item.user))

        // const newUsers = [
        //     ...users,
        // ]
        //
        // data.map(item => setUsers())
        //
        // const a = Array.from(new Set(users));
        // a.length
    }

    return <div className = {style.header}>
        <span className={style.name}>
            My chat
        </span>
        <span className = {style.participants}>
            5 participants
        </span>
        <span className = {style.participants}>
            {data.length} messages
        </span>
        <span className = {style.time}>
            last message at 20.22
        </span>
    </div>
}