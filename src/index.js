import React from 'react';
import ReactDOM from 'react-dom';
import {Chat} from './chat/chat';
import style from "./index.css";

ReactDOM.render(
    <Chat />,
  document.getElementById('root')
);

