import React, {useState} from "react";
import style from "./message-input.css";

export const MessagesInput = ({data, setData, myMessages, setMyMessages}) => {

    const [value, setValue] = useState('');

    const onChange = (e) => {
        setValue(e.target.value);
    }

    const addMessage = (text) => {
        const newMessage = {
            id: 23,
            text,
            user: 'Vasyl'
        }

        const newData = [
            ...data,
            newMessage
        ]

        const newMyMessages = [
            ...myMessages,
            newMessage
        ]

        setData(newData);
        setMyMessages(newMyMessages);

        setValue('');
    }

    return <div className = {style.messagesInput}>
        <input
            className = {style.input}
            onChange = {onChange}
            value = {value}
            placeholder = "Enter your message" />
        <button
            className = {style.btn}
            onClick = {() => addMessage(value)}
        > Send </button>
    </div>
}