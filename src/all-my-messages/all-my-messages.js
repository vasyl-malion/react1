import React, {useState} from "react";
import {MyMessage} from "../my-message/my-message";

export const AllMyMessages = () => {

    const [myMessages, setMyMessages] = useState([])

    return <div>
        {
            myMessages.map(item => <MyMessage data = {item} />)
        }
    </div>
}