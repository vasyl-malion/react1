import React, {useState} from "react";
import style from "./message.css";
import {AiFillLike} from "react-icons/ai"

export const Message = ({src, text, createdAt}) => {

    const [like, setLike] = useState(false);
    let clazz = style.like

    const pressLike = () => {
        setLike(!like)
    }

    if (like) {
        clazz = style.likeActive
    }

    return <div className={style.message}>
        <img src = {src} className = {style.avatar} />
        <span className = {style.text} > {text} </span>
        <span className = {clazz} onClick = {pressLike} >
            <AiFillLike />
        </span>
        <span className = {style.createdAt} > {createdAt} </span>
    </div>
}