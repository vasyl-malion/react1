import React from "react";
import style from "./loading.css";

export const Loading = () => {

    return <span className = {style.loading}>
        Loading...
    </span>
}