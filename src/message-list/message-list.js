import React from "react";
import {Message} from "../message/message";
import style from './message-list.css';

export const MessagesList = ({data}) => {

    return <div className = {style.list}>
        {data.map( item => <Message
            key = {data.id}
            src = {item.avatar}
            text={item.text}
            createdAt = {item.createdAt}
        />)}
    </div>
}