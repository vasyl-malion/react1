import React from "react";
import style from "./my-message.css";

export const MyMessage = ({data}) => {

    return <div className={style.myMessage}>
        {/*<img src = {data.src} className = {style.avatar} />*/}
        <span className = {style.text} > {data.text} </span>

    </div>
}