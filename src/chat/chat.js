import React, {useState, useEffect} from "react";
import {Header} from "../header/header";
import {MessagesList} from "../message-list/message-list";
import {MessagesInput} from "../message-input/message-input";
import {Loading} from "../loading/loading";

export const Chat = () => {

    const [data, setData] = useState([]);
    const [myMessages, setMyMessages] = useState([]);
    const [loading, setLoading] = useState(true);
    const [allM, setAllM] = useState()

    useEffect(async () => {
        await fetch('https://api.npoint.io/b919cb46edac4c74d0a8')
            .then( res => res.json())
            .then( data => setData(data))

        setLoading(false)
    }, [])

    if (loading) {
        return <Loading />
    }

    return <React.Fragment>
        <Header data = {data} />
        <MessagesList data = {data} />
        <MessagesInput
            data = {data}
            setData = {setData}
            myMessages = {myMessages}
            setMyMessages = {setMyMessages} />
    </React.Fragment>
}